/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 *
 * @author hasee
 */
public class ListFrame extends javax.swing.JFrame {

    private final List<String> files;

    // Create the StyleContext, the document and the pane
    StyleContext sc = new StyleContext();
    DefaultStyledDocument doc;
    // Create and add the style
    final Style heading2Style = sc.addStyle("Heading2", null);

    /**
     * Creates new form ListFrame
     *
     * @param files
     */
    public ListFrame(List<String> files) {
        initComponents();

        heading2Style.addAttribute(StyleConstants.Foreground, Color.red);
        heading2Style.addAttribute(StyleConstants.FontSize, new Integer(16));
        heading2Style.addAttribute(StyleConstants.FontFamily, "serif");
        heading2Style.addAttribute(StyleConstants.Bold, new Boolean(true));

        this.files = files;
        this.loadData();
        //System.out.println("");
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) {
                    return;
                }

                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                if (lsm.isSelectionEmpty()) {
                    System.out.println("No rows are selected.");
                } else {
                    int selectedRow = lsm.getMinSelectionIndex();
                    String text = getFileContents(files.get(selectedRow));
                    // jTextPane1.setText(text);

                    List<String> keywords = MainScreen.KEYWORDS;
                    //    System.out.println(keywords);

                    try {
                        updateText(text, keywords);
                    } catch (Exception ex) {
                        Logger.getLogger(ListFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

    }
    
     private void highlight(JTextComponent textComp, String pattern)
      throws Exception {
   // removeHighlights(textComp);

    Highlighter hilite = textComp.getHighlighter();
    Document doc = textComp.getDocument();
    String text = doc.getText(0, doc.getLength());
    int pos = 0;

    DefaultHighlightPainter highlightPainter = new DefaultHighlightPainter(Color.YELLOW);
    while ((pos = text.indexOf(pattern, pos)) >= 0) {
      hilite.addHighlight(pos, pos + pattern.length(), highlightPainter);
      pos += pattern.length();
    }

  }
    
    private void updateText(String text, List<String> keywords) throws Exception {

        jTextPane1.setText(text);
        Document document = jTextPane1.getDocument();

        jTextPane1.setCaretPosition(0);
        
//        for (String keyword : keywords) {
//            this.highlight(jTextPane1, keyword);
//        }

        List<String> words = Arrays.asList(text.split(" "));
        //System.out.println(words);
        for (String keyword : keywords) {
            for (int index = 0; index + keyword.length() < document.getLength(); index++) {
                
                String match = document.getText(index, keyword.length());
                
               // System.out.println(match);
                
                // String match = keyword;
                if (keyword.equalsIgnoreCase(match) && words.contains(match)) {

                   // System.out.println("Found keyword: " + keyword);

                    DefaultHighlightPainter highlightPainter = new DefaultHighlightPainter(Color.YELLOW);
                    jTextPane1.getHighlighter().addHighlight(index, index + keyword.length(), highlightPainter);
                }
            }
        }
    }

    private String getFileContents(String path) {
        StringBuilder sb = new StringBuilder();
        try {
            Scanner input = new Scanner(new File(path));
            while (input.hasNextLine()) {
                sb.append(input.nextLine());
                sb.append("\n");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

    private void loadData() {
        //String[] filesArray = files.toArray(new String[files.size()]);
        Vector data = new Vector();
        for (String file : files) {
            Vector aVecor = new Vector();

            String name = new File(file).getName();
            aVecor.add(name);
            data.add(aVecor);
        }
        Vector names = new Vector();
        names.add("Classes");
        TableModel model = new DefaultTableModel(data, names);
        table.setModel(model);
//        System.out.println("Files: "+ filesArray.length);
//        for (int i = 0; i < filesArray.length; i++) {
//            table.setValueAt(filesArray[i], i, 0);
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Classes Found"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(table);

        jTextPane1.setEditable(false);
        jScrollPane3.setViewportView(jTextPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
            .addComponent(jScrollPane3)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
