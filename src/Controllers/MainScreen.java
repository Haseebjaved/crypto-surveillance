package Controllers;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class MainScreen extends javax.swing.JFrame {

    JFileChooser chooser;
    String choosertitle;
    public static File folder;
    FileReader fr = null;
    static String temp = "";
    public static List<String> filteredList = new ArrayList();
    public static List<String> keywordsList = new ArrayList();
    JProgressBar progressBar = new JProgressBar();

    public static final List<String> KEYWORDS = new ArrayList();

    static {
        try {
             String dir = System.getProperty("user.dir") + "/Keywords.txt";
             dir = dir.replaceAll("/", File.separator);
        //System.out.println(dir);
            
            Scanner input = new Scanner(new File(dir));
            while (input.hasNextLine()) {
                KEYWORDS.add(input.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MainScreen() {
        initComponents();

        jProgressBar1.setBackground(new Color(0, 255, 0, 127));
        jProgressBar1.setIndeterminate(true);
        jProgressBar1.setVisible(false); //to show
        
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jButton1.setText("Select Main Folder");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Start Searching");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Papyrus", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(185, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(196, 196, 196))
            .addGroup(layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(jButton1)
                .addGap(37, 37, 37)
                .addComponent(jButton2)
                .addGap(44, 44, 44)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(jLabel1)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle(choosertitle);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

            Path path = new File("" + chooser.getSelectedFile().getAbsolutePath()).toPath();
            //JOptionPane.showMessageDialog(null, "" + path, "Test", JOptionPane.INFORMATION_MESSAGE);

            String[] extensions = new String[]{"txt", "java"};
            List<String> pathsList = getFileNames(new ArrayList<>(), path, extensions);

            filteredList = new ArrayList<>();
            for (String file : pathsList) {
                if (file.endsWith(".java") || file.endsWith(".txt")) {
                    filteredList.add(file);
                }
            }

            // this.showTable(filteredList);
             System.out.println("Files found " + filteredList.size());
            JOptionPane.showMessageDialog(null,filteredList.size()+" java files found" , "Done", JOptionPane.INFORMATION_MESSAGE);
//        for (String file:filteredList) {
//        System.out.println(file);
//        }
        } else {
            JOptionPane.showMessageDialog(null, "Error selecting directory", "ooopps", JOptionPane.INFORMATION_MESSAGE);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param files
     */
    public void showTable(List<String> files) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        ListFrame frame = new ListFrame(files);
        frame.setSize(800, 600);
        //frame.setMaximizedBounds(frame.getBounds());
        //System.out.println("Showing...");
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
//        java.awt.EventQueue.invokeLater(() -> {
//            ListFrame frame = new ListFrame(files);
//            frame.setSize(800, 600);
//            //frame.setMaximizedBounds(frame.getBounds());
//            System.out.println("Showing...");
//            frame.setLocationRelativeTo(null);
//            frame.setVisible(true);
//        });
    }

    private static List<String> getFileNames(List<String> fileNames, Path dir, String... extensions) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path path : stream) {
                if (path.toFile().isDirectory()) {
                    getFileNames(fileNames, path, extensions);
                } else {
                    fileNames.add(path.toAbsolutePath().toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileNames;
    }


    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        SwingUtilities.invokeLater(() -> {
            jProgressBar1.setVisible(true);
        });

        // Runs outside of the Swing UI thread
        new Thread(() -> {

            /**/ jLabel1.setText("Searching through all the files");
            Writer output;
            PrintWriter writer = null;
            String path = System.getProperty("user.home") + "/Desktop/results.txt";
            path = path.replaceAll("/", File.separator);
            File file = new File(path);
            try {
                output = new BufferedWriter(new FileWriter(file));
                writer = new PrintWriter(file, "UTF-8");
            } catch (IOException ex) {
                Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
            }
            // System.out.println(KEYWORDS);

            int FileCounter = 1;

            // empty keywords list first
            keywordsList = new ArrayList();
            for (String link : filteredList) {
                //System.out.println("File " + FileCounter++ + ": " + link);
                writer.println("File " + FileCounter++ + ": " + link);
                try {
                    fr = new FileReader("" + link);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
                }
                BufferedReader br = new BufferedReader(fr);
                String s;
                int linecount = 0;
                try {
                    while ((s = br.readLine()) != null) {
                        linecount = linecount + 1;
                        String[] line = s.split(" ");
                        for (String word : line) {
                            if (KEYWORDS.contains(word.replaceAll(" ", "").toUpperCase())) {

                                if (!keywordsList.contains(link)) {
                                    keywordsList.add(link);
                                }

                                writer.println("word is " + word + " and line is \" " + s + " \" and line number is " + linecount);
//                            System.out.println("word is " + word + " and line is \" " + s + " \" and line number is " + linecount);
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MainScreen.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            writer.close();
            jLabel1.setText(" ");
//            JOptionPane.showMessageDialog(null, "Finished searching", "OK", JOptionPane.INFORMATION_MESSAGE);

            // Runs inside of the Swing UI thread
            SwingUtilities.invokeLater(() -> {
                jProgressBar1.setVisible(false);

                this.showTable(keywordsList);
            });

        }).start();

    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainScreen().setVisible(true);
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}
